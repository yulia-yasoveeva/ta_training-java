package com.epam.training.iulia_iasoveeva.wd.bring_it_on;

import com.epam.training.iulia_iasoveeva.wd.BaseTest;
import com.epam.training.iulia_iasoveeva.wd.hello_webdriver.Expiration;
import com.epam.training.iulia_iasoveeva.wd.hello_webdriver.PastebinHomePage;
import com.epam.training.iulia_iasoveeva.wd.hello_webdriver.PastebinResultPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateNewPasteWithAttributes extends BaseTest {
  private static final String CODE =
      "git config --global user.name  \"New Sheriff in Town\"\n"
          + "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n"
          + "git push origin master --force";
  private static final String TITLE = "how to gain dominance among developers";
  private static final String SYNTAX = "Bash";
  private PastebinResultPage resultPage;

  @BeforeClass(description = "Create new Paste")
  public void createNewPaste() {
    resultPage =
        new PastebinHomePage(driver)
            .openPage()
            .agreePrivacyAgreement()
            .writePaste(CODE)
            .chooseHighlighting(SYNTAX)
            .chooseExpiration(Expiration.TEN_MINUTES.getValue())
            .writeTitle(TITLE)
            .submitAndWaitCreation();
  }

  @Test(description = "Check new paste Title")
  private void createPasteWithTitle() {
    String pageTitle = driver.getTitle();
    Assert.assertTrue(pageTitle.contains(TITLE), "Actual Title is not correct");
  }

  @Test(description = "Check new paste Syntax")
  private void createPasteWithSyntax() {
    String messageSyntax = resultPage.getPasteSyntax();
    Assert.assertEquals(messageSyntax, SYNTAX, "Actual Syntax is not " + SYNTAX);
  }

  @Test(description = "Check new paste Code")
  private void createPasteWithCode() {
    String messageCode = resultPage.getPasteCode().trim();
    Assert.assertEquals(messageCode, CODE, "Actual Code is not as expected");
  }
}
