package com.epam.training.iulia_iasoveeva.wd;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
  protected WebDriver driver;

  @BeforeTest
  public void browserSetUp() {
    WebDriverManager.chromedriver().setup();
    driver = new ChromeDriver();
    driver.manage().window().maximize();
  }

  @AfterTest(alwaysRun = true)
  public void browserQuit() {
    driver.quit();
    driver = null;
  }
}
