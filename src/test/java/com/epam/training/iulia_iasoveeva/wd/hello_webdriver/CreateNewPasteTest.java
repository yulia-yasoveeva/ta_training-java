package com.epam.training.iulia_iasoveeva.wd.hello_webdriver;

import com.epam.training.iulia_iasoveeva.wd.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateNewPasteTest extends BaseTest {
  private static final String CODE = "Hello from WebDriver";
  private static final String TITLE = "helloweb";
  private static final String MESSAGE = "Your guest paste has been posted.";

  @Test(description = "Create new paste")
  private void createNewPaste() {
    PastebinResultPage resultPage =
        new PastebinHomePage(driver)
            .openPage()
            .agreePrivacyAgreement()
            .writePaste(CODE)
            .chooseExpiration(Expiration.TEN_MINUTES.getValue())
            .writeTitle(TITLE)
            .submitAndWaitCreation();

    String message = resultPage.getConfirmationMessage();
    Assert.assertTrue(message.contains(MESSAGE));
  }
}
