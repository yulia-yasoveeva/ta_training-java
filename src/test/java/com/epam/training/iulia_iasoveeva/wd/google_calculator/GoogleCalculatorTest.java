package com.epam.training.iulia_iasoveeva.wd.google_calculator;

import com.epam.training.iulia_iasoveeva.wd.BaseTest;
import org.openqa.selenium.WindowType;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class GoogleCalculatorTest extends BaseTest {
  private static final String SEARCH_TEXT = "Google Cloud Platform Pricing Calculator";
  private static final String CALCULATOR_TITLE = "Google Cloud Pricing Calculator";
  private static final int NUMBER_OF_INSTANCES = 4;
  private static final String PRICE_REGEX = "\\d*,*\\d{1,3}.\\d{2}";
  private static final String TOTAL_ESTIMATION_COST_REGEX =
      String.format("Total Estimated Cost: USD %s per 1 month", PRICE_REGEX);
  private static final String OPERATING_SYSTEM =
      "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)";
  private static final String PROVISIONING_MODEL = "Regular";
  private static final String MACHINE_FAMILY = "General purpose";
  private static final String MACHINE_TYPE = "n1-standard-8 (vCPUs: 8, RAM: 30GB)";
  private static final String SERIES = "N1";
  private static final String GPU_TYPE = "NVIDIA Tesla T4";
  private static final String LOCAL_SSD = "2x375 GB";
  private static final String DATA_LOCATION = "Frankfurt (europe-west3)";
  private static final String COMMITTED_USAGE = "1 Year";
  private static final int GPU_NUMBER = 1;
  private GoogleCalculatorPage googleCalculatorPage;
  private String totalCost;
  private String originalHandle;

  @BeforeClass(description = "Perform estimate with attributes")
  public void performSearchWithAttributes() {
    googleCalculatorPage =
        new GoogleCloudMainPage(driver)
            .openPage()
            .performSearchByText(SEARCH_TEXT)
            .openSearchResult(CALCULATOR_TITLE)
            .closeCookiesMessage()
            .switchToIframe()
            .fillNumberOfInstances(NUMBER_OF_INSTANCES)
            .chooseOperatingSystem(OPERATING_SYSTEM)
            .chooseProvisioningModel(PROVISIONING_MODEL)
            .chooseMachineFamily(MACHINE_FAMILY)
            .chooseSeries(SERIES)
            .chooseMachineType(MACHINE_TYPE)
            .chooseGPUs(GPU_TYPE)
            .chooseGpuNumber(GPU_NUMBER)
            .chooseLocalSSD(LOCAL_SSD)
            .chooseDataLocation(DATA_LOCATION)
            .chooseCommittedUsage(COMMITTED_USAGE)
            .addToEstimate();
    totalCost = googleCalculatorPage.getTotalCost(PRICE_REGEX);
    originalHandle = driver.getWindowHandle();
  }

  @Test(description = "Calculator estimate")
  private void performCalculatorEstimate() {
    driver.switchTo().window(originalHandle);
    googleCalculatorPage.switchToIframe();
    Assert.assertTrue(
        googleCalculatorPage.getCalculationResult().matches(TOTAL_ESTIMATION_COST_REGEX),
        "Total cost should be displayed");
  }

  @Test(description = "Email estimate")
  private void performEmailEstimate() {
    driver.switchTo().newWindow(WindowType.TAB);
    String mailHandle = driver.getWindowHandle();
    String email =
        new YopmailMainPage(driver)
            .openPage()
            .agreeCookiesMessage()
            .clickRandomEmailGenerator()
            .getGeneratedEmail();
    driver.switchTo().window(originalHandle);
    googleCalculatorPage
        .switchToIframe()
        .clickEmailEstimate()
        .switchToIframe()
        .enterEmail(email)
        .clickSendEmail();
    driver.switchTo().window(mailHandle);
    YopmailCheckInboxPage inboxPage =
        new YopmailResultPage(driver).clickCheckInbox().waitUntilEmailAppear();
    Assert.assertEquals(
        inboxPage.getTotalCost(PRICE_REGEX),
        totalCost,
        "Total cost in the Email should be the same as in calculator");
  }
}
