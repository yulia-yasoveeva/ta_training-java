package com.epam.training.iulia_iasoveeva.wd.hello_webdriver;

public enum Expiration {
  NEVER("Never"),
  BURN_AFTER_DEAD("Burn after read"),
  TEN_MINUTES("10 Minutes"),
  ONE_HOUR("1 Hour"),
  ONE_DAY("1 Day"),
  ONE_WEEK("1 Week"),
  TWO_WEEKS("2 Weeks"),
  ONE_MONTH("1 Month"),
  SIX_MONTHS("6 Months"),
  ONE_YEAR("1 Year");

  private final String value;

  public String getValue() {
    return value;
  }

  private Expiration(String value) {
    this.value = value;
  }
}
