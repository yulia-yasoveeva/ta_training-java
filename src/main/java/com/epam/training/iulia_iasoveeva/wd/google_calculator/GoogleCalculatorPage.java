package com.epam.training.iulia_iasoveeva.wd.google_calculator;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleCalculatorPage extends BasePage {
  private static final By INSTANCES_INPUT = By.id("input_98");
  private static final By AGREE_COOKIE_BUTTON =
      By.xpath("//button[@class='devsite-snackbar-action']");
  private static final String DROPDOWN_VALUE = "//md-option/div[contains(text(),'%s')]";
  private static final By OPERATING_SYSTEM_DROPDOWN = By.id("select_value_label_90");
  private static final By PROVISIONING_MODEL_DROPDOWN = By.id("select_value_label_91");
  private static final By MACHINE_FAMILY_DROPDOWN = By.id("select_value_label_92");
  private static final By SERIES_DROPDOWN = By.id("select_value_label_93");
  public static final By OUTER_FRAME = By.xpath("//iframe[contains(@name, 'goog_')]");
  public static final By INNER_FRAME = By.id("myFrame");
  private static final By MACHINE_TYPE_DROPDOWN = By.id("select_value_label_94");
  private static final By GPU_CHECKBOX =
      By.xpath("//md-checkbox[@ng-model=\"listingCtrl.computeServer.addGPUs\"]");
  private static final By GPU_TYPE_DROPDOWN = By.id("select_505");
  private static final By GPU_NUMBER_DROPDOWN = By.id("select_value_label_504");
  private static final String GPU_NUMBER_VALUES =
      "//div[contains(@class,\"md-select-menu-container md-active md-clickable\")]/md-select-menu//md-option[@value=\"%d\"]";
  private static final By LOCAL_SSD_DROPDOWN = By.id("select_value_label_463");
  private static final By DATA_LOCATION_DROPDOWN = By.id("select_value_label_96");
  private static final By COMMITTED_USAGE_DROPDOWN = By.id("select_value_label_97");
  private static final By ADD_TO_ESTIMATE_BUTTON =
      By.xpath("//button[@ng-click=\"listingCtrl.addComputeServer(ComputeEngineForm);\"]");
  private static final By ESTIMATION_RESULT_COST = By.xpath("//h2/b[@class=\"ng-binding\"]");
  private static final By EMAIL_ESTIMATE_BUTTON = By.xpath("//button[@id=\"Email Estimate\"]");
  private static final By EMAIL_INPUT = By.xpath("//input[@type=\"email\"]");

  public GoogleCalculatorPage(WebDriver driver) {
    super(driver);
  }

  public GoogleCalculatorPage switchToIframe() {
    WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10L));
    driver.switchTo().defaultContent();
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(OUTER_FRAME));
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(INNER_FRAME));
    return this;
  }

  public GoogleCalculatorPage closeCookiesMessage() {
    driver.findElement(AGREE_COOKIE_BUTTON).click();
    return this;
  }

  public GoogleCalculatorPage fillNumberOfInstances(int number) {
    driver.findElement(INSTANCES_INPUT).sendKeys(String.valueOf(number));
    return this;
  }

  public GoogleCalculatorPage chooseOperatingSystem(String operatingSystem) {
    WebElement operatingSystemDropdown = driver.findElement(OPERATING_SYSTEM_DROPDOWN);
    operatingSystemDropdown.click();
    chooseValueOfDropdown(operatingSystem);
    return this;
  }

  public GoogleCalculatorPage chooseProvisioningModel(String provisioningModel) {
    WebElement provisioningModelDropdown = driver.findElement(PROVISIONING_MODEL_DROPDOWN);
    provisioningModelDropdown.click();
    chooseValueOfDropdown(provisioningModel);
    return this;
  }

  public GoogleCalculatorPage chooseMachineFamily(String machineFamily) {
    WebElement machineFamilyDropdown = driver.findElement(MACHINE_FAMILY_DROPDOWN);
    machineFamilyDropdown.click();
    chooseValueOfDropdown(machineFamily);
    return this;
  }

  public GoogleCalculatorPage chooseSeries(String series) {
    WebElement seriesDropdown = driver.findElement(SERIES_DROPDOWN);
    seriesDropdown.click();
    chooseValueOfDropdown(series);
    return this;
  }

  public GoogleCalculatorPage chooseMachineType(String machineType) {
    WebElement machineTypeDropdown = driver.findElement(MACHINE_TYPE_DROPDOWN);
    machineTypeDropdown.click();
    chooseValueOfDropdown(machineType);
    return this;
  }

  public GoogleCalculatorPage chooseGPUs(String gpuType) {
    WebElement gpuCheckBox = driver.findElement(GPU_CHECKBOX);
    waitForElementIsClickable(gpuCheckBox).click();
    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", gpuCheckBox);
    WebElement gpuDropdown = driver.findElement(GPU_TYPE_DROPDOWN);
    waitForElementIsClickable(gpuDropdown).click();
    chooseValueOfDropdown(gpuType);
    return this;
  }

  public GoogleCalculatorPage chooseGpuNumber(int numberGPU) {
    WebElement gpuNumberDropdown = driver.findElement(GPU_NUMBER_DROPDOWN);
    waitForElementIsClickable(gpuNumberDropdown).click();
    new WebDriverWait(driver, Duration.ofSeconds(1))
        .until(
            ExpectedConditions.presenceOfElementLocated(
                By.xpath(String.format(GPU_NUMBER_VALUES, numberGPU))))
        .click();
    return this;
  }

  public GoogleCalculatorPage chooseLocalSSD(String localSSD) {
    WebElement localSSDDropdown = driver.findElement(LOCAL_SSD_DROPDOWN);
    waitForElementIsClickable(localSSDDropdown).click();
    chooseValueOfDropdown(localSSD);
    return this;
  }

  public GoogleCalculatorPage chooseDataLocation(String dataLocation) {
    WebElement dataLocationDropdown = driver.findElement(DATA_LOCATION_DROPDOWN);
    waitForElementIsClickable(dataLocationDropdown).click();
    chooseValueOfDropdown(dataLocation);
    return this;
  }

  public GoogleCalculatorPage chooseCommittedUsage(String committedUsage) {
    WebElement committedUsageDropdown = driver.findElement(COMMITTED_USAGE_DROPDOWN);
    committedUsageDropdown.click();
    chooseValueOfDropdown(committedUsage);
    return this;
  }

  public GoogleCalculatorPage addToEstimate() {
    WebElement addToEstimateButton = driver.findElement(ADD_TO_ESTIMATE_BUTTON);
    addToEstimateButton.click();
    return this;
  }

  public String getCalculationResult() {
    WebElement estimationResultCost = driver.findElement(ESTIMATION_RESULT_COST);
    return estimationResultCost.getText();
  }

  public GoogleCalculatorPage clickEmailEstimate() {
    WebElement emailEstimateButton = driver.findElement(EMAIL_ESTIMATE_BUTTON);
    waitForElementIsClickable(emailEstimateButton).click();
    return this;
  }

  public GoogleCalculatorPage enterEmail(String email) {
    WebElement emailValueInput = driver.findElement(EMAIL_INPUT);
    emailValueInput.sendKeys(email);
    return this;
  }

  public void clickSendEmail() {
    driver
        .findElement(
            By.xpath(
                "//button[@ng-click=\"emailQuote.emailQuote(true); emailQuote.$mdDialog.hide()\"]"))
        .click();
  }

  public void chooseValueOfDropdown(String value) {
    WebElement dropDownValue = driver.findElement(By.xpath(String.format(DROPDOWN_VALUE, value)));
    waitForElementIsClickable(dropDownValue).click();
  }

  public WebElement waitForElementIsClickable(WebElement element) {
    WebDriverWait waitForElementClickabable = new WebDriverWait(driver, Duration.ofSeconds(10L));
    return waitForElementClickabable.until(ExpectedConditions.elementToBeClickable(element));
  }

  public String getTotalCost(String priceRegex) {
    String calculationResult = getCalculationResult();
    Pattern pattern = Pattern.compile(priceRegex);
    Matcher matcher = pattern.matcher(calculationResult);
    matcher.find();
    return matcher.group(0);
  }
}
