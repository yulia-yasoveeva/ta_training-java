package com.epam.training.iulia_iasoveeva.wd.google_calculator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YopmailMainPage extends BasePage {
  private static final String URL = "https://yopmail.com/";
  private static final By AGREE_COOKIE_BUTTON = By.id("accept");
  private static final By GENERATE_RANDOM_EMAIL =
      By.xpath("//*[@title=\"Generate a random email address\"]");

  public YopmailMainPage(WebDriver driver) {
    super(driver);
  }

  public YopmailMainPage openPage() {
    driver.get(URL);
    return this;
  }

  public YopmailMainPage agreeCookiesMessage() {
    driver.findElement(AGREE_COOKIE_BUTTON).click();
    return this;
  }

  public YopmailResultPage clickRandomEmailGenerator() {
    driver.findElement(GENERATE_RANDOM_EMAIL).click();
    return new YopmailResultPage(driver);
  }
}
