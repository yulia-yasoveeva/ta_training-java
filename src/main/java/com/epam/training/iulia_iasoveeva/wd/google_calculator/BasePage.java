package com.epam.training.iulia_iasoveeva.wd.google_calculator;

import org.openqa.selenium.WebDriver;

public abstract class BasePage {
  protected WebDriver driver;

  protected BasePage(WebDriver driver) {
    this.driver = driver;
  }
}
