package com.epam.training.iulia_iasoveeva.wd.hello_webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PastebinResultPage {
  public static final By COMMENT = By.xpath("//div[@class=\"notice -success -post-view\"]");
  private static final By PAST_TITLE =
      By.xpath("//h1[text()=\"how to gain dominance among developers\"]");
  private static final By PASTE_SYNTAX = By.xpath("//a[text()=\"Bash\"]");
  private static final By PASTE_CODE = By.xpath("//ol");
  final WebDriver driver;

  public PastebinResultPage(WebDriver driver) {
    this.driver = driver;
  }

  public String getConfirmationMessage() {
    WebElement comment = driver.findElement(COMMENT);
    return comment.getText();
  }

  public String getPasteTitle() {
    WebElement comment = driver.findElement(PAST_TITLE);
    return comment.getText();
  }

  public String getPasteSyntax() {
    WebElement comment = driver.findElement(PASTE_SYNTAX);
    return comment.getText();
  }

  public String getPasteCode() {
    WebElement comment = driver.findElement(PASTE_CODE);
    return comment.getText();
  }
}
