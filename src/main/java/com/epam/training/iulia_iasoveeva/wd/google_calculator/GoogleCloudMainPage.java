package com.epam.training.iulia_iasoveeva.wd.google_calculator;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoogleCloudMainPage extends BasePage {
  private static final String URL = "https://cloud.google.com/";
  private static final By SEARCH_ICON = By.xpath("//input[@name=\"q\"]");

  public GoogleCloudMainPage(WebDriver driver) {
    super(driver);
  }

  public GoogleCloudMainPage openPage() {
    driver.get(URL);
    return this;
  }

  public GoogleCloudSearchResultPage performSearchByText(String text) {
    WebElement searchIcon = driver.findElement(SEARCH_ICON);
    searchIcon.click();
    searchIcon.sendKeys(text);
    searchIcon.sendKeys(Keys.ENTER);
    return new GoogleCloudSearchResultPage(driver);
  }
}
