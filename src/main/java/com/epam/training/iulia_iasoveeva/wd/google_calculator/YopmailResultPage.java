package com.epam.training.iulia_iasoveeva.wd.google_calculator;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YopmailResultPage extends BasePage {
  private static final By RANDOM_EMAIL_AREA = By.xpath("//*[@id=\"egen\"]");
  private static final By CHECK_INBOX_BUTTON = By.xpath("//button/span[text() = \"Check Inbox\"]");

  public YopmailResultPage(WebDriver driver) {
    super(driver);
  }

  public String getGeneratedEmail() {
    WebElement randomEmailValue = driver.findElement(RANDOM_EMAIL_AREA);
    return randomEmailValue.getText();
  }

  public YopmailCheckInboxPage clickCheckInbox() {
    ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,150)");
    WebElement checkInboxButton = driver.findElement(CHECK_INBOX_BUTTON);
    checkInboxButton.click();
    return new YopmailCheckInboxPage(driver);
  }
}
