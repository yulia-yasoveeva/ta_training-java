package com.epam.training.iulia_iasoveeva.wd.google_calculator;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class YopmailCheckInboxPage extends BasePage {
  private static final By EMAIL_TEXT = By.xpath("//div[@id=\"mail\"]");
  private static final By TOTAL_COST =
      By.xpath("//h3[text()=\"Total Estimated Monthly Cost\"]/parent::td/following-sibling::td");
  private static final By IFRAME = By.xpath("//*[@name=\"ifmail\"]");

  public YopmailCheckInboxPage(WebDriver driver) {
    super(driver);
  }

  public YopmailCheckInboxPage waitUntilEmailAppear() {
    Wait<WebDriver> wait =
        new FluentWait<>(driver)
            .withTimeout(Duration.ofMinutes(1))
            .pollingEvery(Duration.ofSeconds(10))
            .ignoring(NoSuchElementException.class);
    driver.navigate().refresh();
    wait.until(
        webDriver -> {
          webDriver.navigate().refresh();
          wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(IFRAME));
          return driver.findElement(EMAIL_TEXT);
        });
    return this;
  }

  public String getMessage() {
    return driver.findElement(TOTAL_COST).getText();
  }

  public String getTotalCost(String priceRegex) {
    Pattern pattern = Pattern.compile(priceRegex);
    Matcher matcher = pattern.matcher(getMessage());
    matcher.find();
    return matcher.group(0);
  }
}
