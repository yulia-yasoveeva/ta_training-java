package com.epam.training.iulia_iasoveeva.wd.hello_webdriver;

import static com.epam.training.iulia_iasoveeva.wd.hello_webdriver.PastebinResultPage.COMMENT;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PastebinHomePage {
  private static final String URL = "https://pastebin.com/";
  private static final By AGREE_BUTTON = By.xpath("//button[@mode=\"primary\"]");
  private static final By NEW_PASTE_INPUT = By.xpath("//textarea[@id=\"postform-text\"]");
  private static final By SYNTAX_HIGHLIGHTING_DROPDOWN =
      By.xpath(
          "//label[text()=\"Syntax Highlighting:\"]/..//span[@class=\"select2-selection__arrow\"]");
  private static final String SYNTAX_HIGHLIGHTING_VALUE =
      "//li[contains(@id,'select2-postform-format-result')][text()='%s']";
  private static final By PASTE_EXPIRATION_DROPDOWN =
      By.xpath(
          "//label[text()=\"Paste Expiration:\"]/..//span[@class=\"select2-selection__arrow\"]");
  private static final String PASTE_EXPIRATION_VALUE =
      "//li[contains(@id,'select2-postform-expiration-result')][text()='%s']";
  private static final By PASTE_TITLE = By.xpath("//input[@id=\"postform-name\"]");
  private static final By CREATE_PASTE = By.xpath("//button[@class='btn -big']");
  private final WebDriver driver;

  public PastebinHomePage(WebDriver driver) {
    this.driver = driver;
  }

  public PastebinHomePage openPage() {
    driver.get(URL);
    new WebDriverWait(this.driver, Duration.ofSeconds(10L))
        .until(ExpectedConditions.presenceOfAllElementsLocatedBy(AGREE_BUTTON));
    return this;
  }

  public PastebinHomePage agreePrivacyAgreement() {
    driver.findElement(AGREE_BUTTON).click();
    return this;
  }

  public PastebinHomePage writePaste(String paste) {
    WebElement newPaste = driver.findElement(NEW_PASTE_INPUT);
    newPaste.sendKeys(paste);
    return this;
  }

  public PastebinHomePage chooseHighlighting(String highlightingValue) {
    WebElement highlightingDropdown = driver.findElement(SYNTAX_HIGHLIGHTING_DROPDOWN);
    highlightingDropdown.click();
    ((JavascriptExecutor) driver)
        .executeScript("arguments[0].scrollIntoView(true);", highlightingDropdown);
    new WebDriverWait(this.driver, Duration.ofSeconds(10L))
        .until(ExpectedConditions.presenceOfElementLocated(SYNTAX_HIGHLIGHTING_DROPDOWN));
    driver
        .findElement(By.xpath(String.format(SYNTAX_HIGHLIGHTING_VALUE, highlightingValue)))
        .click();
    return this;
  }

  public PastebinHomePage chooseExpiration(String expirationValue) {

    WebElement expirationDropdown = driver.findElement(PASTE_EXPIRATION_DROPDOWN);
    expirationDropdown.click();
    ((JavascriptExecutor) driver)
        .executeScript("arguments[0].scrollIntoView(true);", expirationDropdown);
    new WebDriverWait(this.driver, Duration.ofSeconds(10L))
        .until(ExpectedConditions.presenceOfElementLocated(PASTE_EXPIRATION_DROPDOWN));
    driver.findElement(By.xpath(String.format(PASTE_EXPIRATION_VALUE, expirationValue))).click();
    return this;
  }

  public PastebinHomePage writeTitle(String phrase) {
    WebElement pasteTitle = driver.findElement(PASTE_TITLE);
    pasteTitle.sendKeys(phrase);
    return this;
  }

  public PastebinResultPage submitAndWaitCreation() {
    WebElement createPaste = driver.findElement(CREATE_PASTE);
    createPaste.click();
    new WebDriverWait(this.driver, Duration.ofSeconds(10L))
        .until(ExpectedConditions.presenceOfAllElementsLocatedBy(COMMENT));
    return new PastebinResultPage(driver);
  }
}
