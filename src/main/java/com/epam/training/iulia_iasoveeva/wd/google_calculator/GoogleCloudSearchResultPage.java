package com.epam.training.iulia_iasoveeva.wd.google_calculator;

import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleCloudSearchResultPage extends BasePage {
  private static final By SEARCH_RESULTS = By.xpath("//div[@class=\"gs-title\"]/a");

  public GoogleCloudSearchResultPage(WebDriver driver) {
    super(driver);
  }

  public GoogleCalculatorPage openSearchResult(String title) {
    List<WebElement> searchResults =
        new WebDriverWait(driver, Duration.ofSeconds(10L))
            .until(ExpectedConditions.presenceOfAllElementsLocatedBy(SEARCH_RESULTS));
    searchResults.stream()
        .filter(webElement -> webElement.getText().equals(title))
        .findFirst()
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Element with title %s not found", title)))
        .click();
    return new GoogleCalculatorPage(driver);
  }
}
